
## Vision Statement

__For__ the benefit of analyzing two large sets of data that on there own may 
say little of the potential probable out comes. Analysts __who__ would like to simulate 
a model of the probable out come of events given the data. __The__ methodology of Bayesian Networks 
__will__ be the focus of the modeling __that__ will lay the foundations of demonstrating the 
ideas of our simulations. __This__ model will have a focus on minimally intersecting data sets and 
our simulation will clearly explain the out puts of the data.