library(shiny)
library(RColorBrewer)

shinyUI(fluidPage(
  
  titlePanel(title = h1("Basic Stats", align="center")),
  
  sidebarLayout(
    sidebarPanel(
      fileInput("inFile1", "input a file 1"),
      fileInput("inFile2", "input a file 2")
      ),
    
    mainPanel(
      uiOutput("tb") # place tabs to display differant data info.
    )
    
  )
))
